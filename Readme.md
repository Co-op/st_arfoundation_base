# AR Foundation base Unity project

no rights reserved, no responsibility taken



## How it was made?

1. [Download Unity 3D](https://unity.com/) and necessary SDKs, NDKs (such as Java&Android) and code editors  (we use Rider but could be VS Code or any other editor. However, only Rider & VS connect to Unity).
2. New Unity 3D project and a scene, changed platform (in our case Android).
3. Project Settings: Auto on graphics API + disable multithreaded rendering + change package identifier + set minimum API level (24)
4. Setting up scene, adding XR components like in [this official introduction](https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@2.0/manual/index.html).
5. Implementing code: *SpawnMoveObject.cs* in Assets/Scripts
6. Build (preview without building to the device still a problem though [Unity devs working on this](https://forum.unity.com/threads/ar-remoting-simulation.720575/))



If you want to explore more complex options of AR Foundation try Unity repo (open it in Unity as a project) https://github.com/Unity-Technologies/arfoundation-samples











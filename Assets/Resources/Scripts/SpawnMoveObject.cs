﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


[RequireComponent(typeof(ARRaycastManager))]
public class SpawnMoveObject : MonoBehaviour
{
    private ARRaycastManager _raycastManager;
    private static List<ARRaycastHit> planeHits = new List<ARRaycastHit>();
    
    
    [SerializeField] private GameObject cubeObject;
    private GameObject _spawnedCube = null;
        private bool _isMoving = false;
    
        private void Awake()
        {
            _raycastManager = GetComponent<ARRaycastManager>();
        }
        
    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            var touch = Input.GetTouch(0);
            
            if (_raycastManager.Raycast(touch.position, planeHits, TrackableType.PlaneWithinPolygon))
            {
                var hitPose = planeHits[0].pose;

                if (_spawnedCube == null) _spawnedCube = Instantiate(cubeObject, hitPose.position, hitPose.rotation);

                else
                {
                    if (!_isMoving)
                    {
                        StartCoroutine(GuideCube(hitPose));
                        _isMoving = true;
                    }
                }
            }
        }
    }
    private IEnumerator GuideCube(Pose hitPose)
    {
        _spawnedCube.transform.LookAt(hitPose.position);

        while(_spawnedCube.transform.position != hitPose.position)
        {
            _spawnedCube.transform.position = Vector3.MoveTowards(_spawnedCube.transform.position, hitPose.position, Time.deltaTime * 1f);
            yield return new WaitForEndOfFrame();
        }
        _isMoving = false; 
    }
        
}
